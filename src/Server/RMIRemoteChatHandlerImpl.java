package Server;

        import java.rmi.RemoteException;
        import java.util.HashMap;

public class RMIRemoteChatHandlerImpl implements RMIRemoteChatHandler {
    private int id=0;
    private HashMap<Integer,String> newMessage = new HashMap <>();
    @Override
    public boolean messageReady (int id) {
        if(newMessage.get(id)==""){
            return false;
        }
        return true;
    }
    @Override
    public String getChatLog (int id) {
        String idMessage = newMessage.get(id);
        newMessage.put(id,"");
        return idMessage;
    }
    @Override
    public void upDateChat (String nick, String message) {
        String chatLog = "<"+nick+"> " + message + "\n";
        for (Integer id : newMessage.keySet()){
            newMessage.put(id,newMessage.get(id)+chatLog);
        }
    }
    @Override
    public int getID () throws RemoteException {
        newMessage.put(id++,"");
        return id;
    }
}
