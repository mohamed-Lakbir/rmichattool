package Server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.RemoteServer;
import java.rmi.server.UnicastRemoteObject;

public class RMIServer {


    //C:\Program Files\Java\jdk-11\bin
    public static void main (String []args) throws RemoteException {

        LocateRegistry.createRegistry(2019);
        RMIRemoteChatHandlerImpl rmiRemoteChatHandlerImpl = new RMIRemoteChatHandlerImpl();
        RMIRemoteChatHandler stub = (RMIRemoteChatHandler) UnicastRemoteObject.exportObject(rmiRemoteChatHandlerImpl,0);
        Registry registry = LocateRegistry.getRegistry(2019);
        registry.rebind("RMIServer",stub);
        System.out.println("Registry angemeldet");
    }
}
