package Server;
        import java.rmi.Remote;
        import java.rmi.RemoteException;

public interface RMIRemoteChatHandler extends Remote {
    public boolean messageReady(int id) throws RemoteException;
    public String getChatLog(int id) throws RemoteException;
    public void upDateChat(String nick, String message) throws  RemoteException;
    public int getID()throws RemoteException;
}
