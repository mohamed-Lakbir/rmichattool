package de.hrw.dsalab.distsys.chat.client;

import java.rmi.RemoteException;
import java.rmi.server.RemoteObjectInvocationHandler;

public interface NetworkListener {
	
	public void messageReceived(String msg);
	public void listenForNewMessage() throws RemoteException, InterruptedException;
	public void sendMessage (String msg) throws RemoteException;
	public void register() throws RemoteException;
}
