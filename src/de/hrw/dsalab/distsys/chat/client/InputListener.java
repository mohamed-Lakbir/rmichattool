package de.hrw.dsalab.distsys.chat.client;

public interface InputListener {
	
	public void inputReceived(String str);

}
