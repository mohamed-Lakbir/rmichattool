package de.hrw.dsalab.distsys.chat.client;

import Server.RMIRemoteChatHandler;

import javax.swing.*;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class RMIRemoteListenerImpl implements  NetworkListener {
    private JTextArea textArea;
    private String nick;
    private RMIRemoteChatHandler remoteChatHandler;
    private int id;

    public RMIRemoteListenerImpl(JTextArea textArea, String nick) throws RemoteException, NotBoundException {
        this.textArea = textArea;
        this.nick = nick;
        Registry registry = LocateRegistry.getRegistry(2019);
        remoteChatHandler = (RMIRemoteChatHandler) registry.lookup("RMIServer");
        register();
    }

    @Override
    public void messageReceived ( String msg ) {
        textArea.append(msg);
    }

    @Override
    public void listenForNewMessage () throws RemoteException, InterruptedException {
        while (true){
            if(remoteChatHandler.messageReady(id)){
                messageReceived(remoteChatHandler.getChatLog(id));
            }
        }
    }
    @Override
    public void sendMessage (String msg) throws RemoteException {
        remoteChatHandler.upDateChat(this.nick,msg);
    }
    @Override
    public void register () throws RemoteException {
        this.id = remoteChatHandler.getID();
    }
}
